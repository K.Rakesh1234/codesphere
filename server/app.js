const dotenv = require('dotenv')
const express = require('express')
const bcryptjs = require('bcryptjs')
const jwt = require('jsonwebtoken')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser');

const cors = require('cors');
const mongoose = require('mongoose');

 
// app.use(cors());
// app.use(express.json());

const app = express();
app.use(cors());
app.use(express.json()); 
app.use(bodyParser.json());

dotenv.config({path : './config.env'});
require('./db/conn')
const port = process.env.PORT

const Users = require('./models/userSchema')
const Message = require('./models/msgSchema')
app.use(express.json())
app.use(express.urlencoded({extended : false}))
app.use(cookieParser());

app.get('/',(req,res) =>{
    res.send("Hello World");

})

app.post('/register', async(req,res) =>{
     try {
        const username = req.body.username;
        const email = req.body.email;
        const password = req.body.password;
   
          const createUser = new Users ({
            username : username,
            email : email,
            password : password
         });
         
         const created = await createUser.save()
         console.log(created);
         res.status(200).send("Registered")

     } catch (error) {
        res.status(400).send(error)
        
     }


})

app.post('/login',async (req, res)=>{
    try {
        const email = req.body.email;
        const password = req.body.password;

        const user = await Users.findOne({email : email})
        if(user){
            // const isMatch =await bcryptjs.compare(password, user.password);
            const isMatch = (password === user.password);
            
            if(isMatch){
                const token = await user.generateToken()
                res.cookie("jwt",token, {
                     expires : new Date(Date.now() +86400000 ),
                     httpOnly :true
                })
                res.status(200).send("Logged in")
            }else{
                res.status(400).send("Invalid Credentials");
            }
        }else{
            res.status(400).send("Invalid Credentials");

        }
        
    } catch (error) {
        res.status(400).send({error : "try not executed"})
        
    }

})

app.post('/message', async(req,res) =>{
    try {
       const name = req.body.name;
       const email = req.body.email;
       const message = req.body.message;
  
         const sendMsg = new Message({
           name : name,
           email : email,
           message : message
        });
        
        const created = await sendMsg.save()
        console.log(created);
        res.status(200).send("Sent")

    } catch (error) {
       res.status(400).send(error)
       
    }


})

app.get('/logout',(req,res) =>{
    res.clearCookie("jwt" , {path : '/'})
    res.status(200).send("User Logged Out")
})

mongoose.connect('mongodb+srv://rakeshreddykalagiri:Rakeshreddy%40123@cluster0.jstbos0.mongodb.net/fineart?retryWrites=true&w=majority',{ useNewUrlParser: true, useUnifiedTopology: true });

app.get('/users', async (req, res) => {
    try {
        const allUsers = await Users.find();
        res.json(allUsers);
    } catch (error) {
        console.error('Error fetching data:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

// Add this route after the '/users' route

app.delete('/user/:username', async (req, res) => {
    const usernameToDelete = req.params.username;

    try {
        const deletedUser = await Users.findOneAndDelete({ username: usernameToDelete });

        if (deletedUser) {
            res.status(200).json({ message: 'User deleted successfully', deletedUser });
        } else {
            res.status(404).json({ message: 'User not found' });
        }
    } catch (error) {
        console.error('Error deleting user:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.put('/update/:username', async (req, res) => {
    const usernameToUpdate = req.params.username;

    try {
        // Find the user by username
        const userToUpdate = await Users.findOne({ username: usernameToUpdate });

        if (userToUpdate) {
            // Update user information
            userToUpdate.username = req.body.username || userToUpdate.username;
            userToUpdate.email = req.body.email || userToUpdate.email;
            userToUpdate.password = req.body.password || userToUpdate.password;

            // Save the updated user
            const updatedUser = await userToUpdate.save();

            res.status(200).json({ message: 'User updated successfully', updatedUser });
        } else {
            res.status(404).json({ message: 'User not found' });
        }
    } catch (error) {
        console.error('Error updating user:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});





app.listen(port,()=>{
 console.log("Server is Listening")

})

